# Dependencies

## Install .NET Core 3.0

https://dotnet.microsoft.com/download/dotnet-core/3.0

## Install Qt/QtCreator

https://www.qt.io/download-qt-installer

## Install protobuf

```
sudo apt-get install libprotobuf-dev
```

## Compile gRCP from source

```
git clone https://github.com/grpc/grpc.git
cd grpc
git submodule update --init
make
make install
```

## Regenerate client-side proto files.

```
./proto-regen.sh
```

# Running

## Server

```
cd server/Server
dotnet run
```

At this point, you now how a gRPC service running on port ```5000``` (unsecured).

## Client

Open client/Client/Client.pro in Qt Creator and "Run".

Type in a name and hit "Send". You will see a response from the server on the GUI.