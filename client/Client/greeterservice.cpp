#include "greeterservice.h"
#include <google/protobuf/service.h>
#include <grpc++/grpc++.h>
#include "proto/greeter.grpc.pb.h"
#include <QDebug>

GreeterService::GreeterService()
{

}

QString GreeterService::sayHello(QString name)
{
    auto channel = grpc::CreateChannel(
                "localhost:5000", grpc::InsecureChannelCredentials());

    auto stub = greet::Greeter::NewStub(channel);

    grpc::ClientContext context;
    greet::HelloRequest request;
    request.set_name(name.toStdString());
    greet::HelloReply reply;

    grpc::Status status = stub->SayHello(&context, request, &reply);

    if(!status.ok()) {
        qWarning("error: %s", status.error_message().c_str());
        return QString::fromStdString(status.error_message());
    }

    return QString::fromStdString(reply.message());
}
