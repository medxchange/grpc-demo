QT += quick quickcontrols2

CONFIG += c++11 link_pkgconfig

DEFINES += QT_DEPRECATED_WARNINGS

PKGCONFIG += protobuf grpc++

HEADERS += proto/greeter.pb.h \
    proto/greeter.grpc.pb.h \
    greeterservice.h

SOURCES += \
        greeterservice.cpp \
        main.cpp \
        proto/greeter.pb.cc \
        proto/greeter.grpc.pb.cc

RESOURCES += qml.qrc
