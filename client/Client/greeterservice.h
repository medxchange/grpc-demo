#ifndef GREETERSERVICE_H
#define GREETERSERVICE_H

#include <QObject>

class GreeterService : public QObject
{
    Q_OBJECT
public:
    GreeterService();

    Q_INVOKABLE QString sayHello(QString name);
};

#endif // GREETERSERVICE_H
