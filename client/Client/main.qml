import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 1.4
import Greeter 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    property string response: ""

    GreeterService {
        id: greeterService
    }

    Column {
        anchors.centerIn: parent

        spacing: 10

        Row {
            spacing: 10

            TextInput {
                id: nameTextInput
                text: "Jim Bob"
                focus: true
                width: 200
            }
            Button {
                text: "Send"
                onClicked: {
                    response = greeterService.sayHello(nameTextInput.text)
                }
            }
        }

        Label {
            text: "Response: " + response
        }
    }


}
