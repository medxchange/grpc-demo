#!/usr/bin/env bash
set -e

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd $SCRIPT_DIR

mkdir ./client/Client/proto || true

protoc -I ./proto --grpc_out=./client/Client/proto --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` ./proto/greeter.proto
protoc -I ./proto --cpp_out=./client/Client/proto ./proto/greeter.proto
